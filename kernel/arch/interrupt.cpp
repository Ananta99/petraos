#include <lib/kstdio.hpp>
#include <lib/kstdlib.hpp>
#include <arch/ports.hpp>
#include <arch/interrupt.hpp>

#define MAX_SIZE 256

extern "C" {
    #define PIC1 0x20
    #define PIC1_COMMAND PIC1
    #define PIC1_DATA (PIC1+1)
    #define PIC1_OFFSET 0x20

    #define PIC2 0xA0
    #define PIC2_COMMAND PIC2
    #define PIC2_DATA (PIC2+1)
    #define PIC2_OFFSET 0x28

    #define PIC_EOI 0x20

    #define ICW1_ICW4 0x01
    #define ICW1_INIT 0x10

    interrupt::IDT idt_entries[MAX_SIZE];
    interrupt::IDT_ptr idt_pointer;

    typedef void (*handler)(registers *current_register);

    handler interrupt_handlers[256] = { 0 };
    handler exception_handlers[32] = { 0 };

    static const char *exception_messages[32] = {
        "Division by zero",
        "Debug",
        "Non-maskable interrupt",
        "Breakpoint",
        "Detected overflow",
        "Out-of-bounds",
        "Invalid opcode",
        "No coprocessor",
        "Double fault",
        "Coprocessor segment overrun",
        "Bad TSS",
        "Segment not present",
        "Stack fault",
        "General protection fault",
        "Page fault",
        "Unknown interrupt",
        "Coprocessor fault",
        "Alignment check",
        "Machine check""Reserved",
        "Reserved",
        "Reserved",
        "Reserved",
        "Reserved",
        "Reserved",
        "Reserved",
        "Reserved",
        "Reserved",
        "Reserved",
        "Reserved",
        "Reserved",
        "Reserved"
    };

    // For Interrupts
    extern void irq0();
    extern void irq1();
    extern void irq2();
    extern void irq3();
    extern void irq4();
    extern void irq5();
    extern void irq6();
    extern void irq7();
    extern void irq8();
    extern void irq9();
    extern void irq10();
    extern void irq11();
    extern void irq12();
    extern void irq13();
    extern void irq14();
    extern void irq15();

    extern void isr0();
    extern void isr1();
    extern void isr2();
    extern void isr3();
    extern void isr4();
    extern void isr5();
    extern void isr6();
    extern void isr7();
    extern void isr8();
    extern void isr9();
    extern void isr10();
    extern void isr11();
    extern void isr12();
    extern void isr13();
    extern void isr14();
    extern void isr15();
    extern void isr16();
    extern void isr17();
    extern void isr18();
    extern void isr19();
    extern void isr20();
    extern void isr21();
    extern void isr22();
    extern void isr23();
    extern void isr24();
    extern void isr25();
    extern void isr26();
    extern void isr27();
    extern void isr28();
    extern void isr29();
    extern void isr30();
    extern void isr31();
}


void exception_handler(registers *current_registers) {
    void (*handler)(registers *current_registers) = exception_handlers[current_registers->int_no];
    if (handler)
        handler(current_registers);
    if (current_registers->int_no< 32) {
        kstdio::kprintf("Exception : %s\n",exception_messages[current_registers->int_no]);
        for(;;);
    }
}

void exception::initialize_exception() {
    interrupt::set_idt_gate(0,(uintptr_t)isr0,0x08,0x8E);
    interrupt::set_idt_gate(1,(uintptr_t)isr1,0x08,0x8E);
    interrupt::set_idt_gate(2,(uintptr_t)isr2,0x08,0x8E);
    interrupt::set_idt_gate(3,(uintptr_t)isr3,0x08,0x8E);
    interrupt::set_idt_gate(4,(uintptr_t)isr4,0x08,0x8E);
    interrupt::set_idt_gate(5,(uintptr_t)isr5,0x08,0x8E);
    interrupt::set_idt_gate(6,(uintptr_t)isr6,0x08,0x8E);
    interrupt::set_idt_gate(7,(uintptr_t)isr7,0x08,0x8E);
    interrupt::set_idt_gate(8,(uintptr_t)isr8,0x08,0x8E);
    interrupt::set_idt_gate(9,(uintptr_t)isr9,0x08,0x8E);
    interrupt::set_idt_gate(10,(uintptr_t)isr10,0x08,0x8E);
    interrupt::set_idt_gate(11,(uintptr_t)isr11,0x08,0x8E);
    interrupt::set_idt_gate(12,(uintptr_t)isr12,0x08,0x8E);
    interrupt::set_idt_gate(13,(uintptr_t)isr13,0x08,0x8E);
    interrupt::set_idt_gate(14,(uintptr_t)isr14,0x08,0x8E);
    interrupt::set_idt_gate(15,(uintptr_t)isr15,0x08,0x8E);
    interrupt::set_idt_gate(16,(uintptr_t)isr16,0x08,0x8E);
    interrupt::set_idt_gate(17,(uintptr_t)isr17,0x08,0x8E);
    interrupt::set_idt_gate(18,(uintptr_t)isr18,0x08,0x8E);
    interrupt::set_idt_gate(19,(uintptr_t)isr19,0x08,0x8E);
    interrupt::set_idt_gate(20,(uintptr_t)isr20,0x08,0x8E);
    interrupt::set_idt_gate(21,(uintptr_t)isr21,0x08,0x8E);
    interrupt::set_idt_gate(22,(uintptr_t)isr22,0x08,0x8E);
    interrupt::set_idt_gate(23,(uintptr_t)isr23,0x08,0x8E);
    interrupt::set_idt_gate(24,(uintptr_t)isr24,0x08,0x8E);
    interrupt::set_idt_gate(25,(uintptr_t)isr25,0x08,0x8E);
    interrupt::set_idt_gate(26,(uintptr_t)isr26,0x08,0x8E);
    interrupt::set_idt_gate(27,(uintptr_t)isr27,0x08,0x8E);
    interrupt::set_idt_gate(28,(uintptr_t)isr28,0x08,0x8E);
    interrupt::set_idt_gate(29,(uintptr_t)isr29,0x08,0x8E);
    interrupt::set_idt_gate(30,(uintptr_t)isr30,0x08,0x8E);
    interrupt::set_idt_gate(31,(uintptr_t)isr31,0x08,0x8E);
}

void exception::exception_uninstall(uint32_t exception_number) {
    exception_handlers[exception_number] = 0;
}

void exception::exception_install(uint32_t exception_number,handler exception) {
    exception_handlers[exception_number] = exception;
}

void interrupt::remap_PIC() {
    outb(PIC1_COMMAND, ICW1_INIT | ICW1_ICW4);
    outb(PIC2_COMMAND, ICW1_INIT | ICW1_ICW4);
    outb(PIC1_DATA, PIC1_OFFSET);
    outb(PIC2_DATA, PIC2_OFFSET);
    outb(PIC1_DATA, 0x04);
    outb(PIC2_DATA, 0x02);
    outb(PIC1_DATA, 0x01);
    outb(PIC2_DATA, 0x01);
}

void interrupt::install_interrupt_handler(int interrupt_number,handler interrupt) {
    interrupt_handlers[interrupt_number] = interrupt;
}

void interrupt::uninstall_interrupt_handler(int interrupt_number) {
    interrupt_handlers[interrupt_number] = 0;
}

void interrupt::initialize_idt() {
    idt_pointer.limit = sizeof(IDT) * MAX_SIZE - 1;
    idt_pointer.base = (uintptr_t)&idt_entries;
    kstdlib::memset(&idt_entries,0,sizeof(IDT) * MAX_SIZE);
    remap_PIC();
    set_idt_gate(32,(uintptr_t)irq0,0x08,0x8E);
    set_idt_gate(33,(uintptr_t)irq1,0x08,0x8E);
    set_idt_gate(34,(uintptr_t)irq2,0x08,0x8E);
    set_idt_gate(35,(uintptr_t)irq3,0x08,0x8E);
    set_idt_gate(36,(uintptr_t)irq4,0x08,0x8E);
    set_idt_gate(37,(uintptr_t)irq5,0x08,0x8E);
    set_idt_gate(38,(uintptr_t)irq6,0x08,0x8E);
    set_idt_gate(39,(uintptr_t)irq7,0x08,0x8E);
    set_idt_gate(40,(uintptr_t)irq8,0x08,0x8E);
    set_idt_gate(41,(uintptr_t)irq9,0x08,0x8E);
    set_idt_gate(42,(uintptr_t)irq10,0x08,0x8E);
    set_idt_gate(43,(uintptr_t)irq11,0x08,0x8E);
    set_idt_gate(44,(uintptr_t)irq12,0x08,0x8E);
    set_idt_gate(45,(uintptr_t)irq13,0x08,0x8E);
    set_idt_gate(46,(uintptr_t)irq14,0x08,0x8E);
    set_idt_gate(47,(uintptr_t)irq15,0x08,0x8E);
    asm volatile("lidt (%0)" :: "r"(&idt_pointer));
}

void interrupt::set_idt_gate(uint8_t num,uint32_t base,uint16_t selector,uint16_t flags) {
    idt_entries[num].offset_lower = (uint16_t)base;
    idt_entries[num].offset_mid = (uint16_t)(base >> 16);
    idt_entries[num].offset_higher = (uint32_t)(base >> 32);
    idt_entries[num].selector = selector;
    idt_entries[num].always_zero = 0;
    idt_entries[num].attribute_type = flags;
}

void interrupt_handler(registers *current_register) {
    handler current_handler = interrupt_handlers[current_register->int_no - 32];
    if (current_handler)
        current_handler(current_register);
    if (current_register->int_no >= 40)
        outb(0xA0,0x20);
    outb(0x20,0x20);
}
