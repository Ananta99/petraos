#include <arch/pit.hpp>
#include <arch/ports.hpp>
#include <lib/kstdio.hpp>

extern "C" {
    uint32_t ticker = 0;
}

void PIT::pit_callback(registers *current_register) {
    ticker++;
    kstdio::kprintf("Timer : %d",ticker);
    kstdio::kprintf("register rbp : %d\n",current_register->rbp);
}

void PIT::initialized_pit() {
    interrupt::install_interrupt_handler(0,pit_callback);
    uint32_t divisor = 1193180 / 50;
    outb(0x43,0x36);
    outb(0x40,divisor & 0xFF);
    outb(0x40,divisor >> 8);
}
