#include <arch/pit.hpp>
#include <arch/gdt.hpp>
#include <drivers/terminal.hpp>

typedef void (*constructor)();
extern "C" constructor start_constructor;
extern "C" constructor end_constructor;
extern "C" void call_constructor() {
    for (constructor *iterator = &start_constructor; iterator != &end_constructor; iterator++)
        (*iterator)();
}

extern "C" void kmain() {
    Terminal terminal;
    terminal.clear_screen();
    GDT::initialize_gdt();
    interrupt::initialize_idt();
    exception::initialize_exception();
    PIT::initialized_pit();
    asm("int $3");
}