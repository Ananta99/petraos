#include <lib/kstdlib.hpp>

void *kstdlib::memset(void *destination,int value, size_t size) {
    unsigned char *destination_temp = (unsigned char *)destination;
    for (size_t i = 0; i < size; i++)
        destination_temp[i] = value;
    return destination;
}

void *kstdlib::memcpy(void *destination, void *source,size_t size) {
    unsigned char *destination_temp = (unsigned char *)destination;
    unsigned char *source_temp = (unsigned char*)source;
    for (size_t i = 0; i < size; i++)
        destination_temp[i] = source_temp[i];
    return source;
}

void *kstdlib::memmove(void *destination, void *source,size_t size) {
    unsigned char *destination_temp = (unsigned char *)destination;
    unsigned char *source_temp = (unsigned char *)source;
    for (size_t i = 0; i < size; i++)
        destination_temp[i] = source_temp[i];
     for (size_t i = 0; i < size; i++)
        source_temp[i] = 0;
    return destination;   
}