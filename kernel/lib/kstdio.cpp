#include <lib/kstdio.hpp>
#include <drivers/terminal.hpp>

extern "C" {
    Terminal terminal_text;
}

void kstdio::kprintf_unsigned(uint32_t value) {
    int multiple_of_ten, decimal_number;
    multiple_of_ten = 1;
    while ((value / multiple_of_ten) >= 10)
        multiple_of_ten *= 10;
    while (value != 0) {
        decimal_number = value / multiple_of_ten;
        terminal_text.terminal_putchar('0' + decimal_number);
        value = value - (decimal_number * multiple_of_ten);
        multiple_of_ten /= 10;
    }
}

void kstdio::kprintf_int(int32_t value) {
    if (value < 0) {
        terminal_text.terminal_putchar('-');
        value -= value;
    }
    kprintf_unsigned(value);
}

void kstdio::kprintf_hex(int32_t value) {
    for (int j = 28; j >= 0; j--) {
        uint8_t current = (value >> j) & 0x0F;
        if (current < 10) 
            terminal_text.terminal_putchar(current);
        else
            terminal_text.terminal_putchar('a' + current - 10);
    }
}

void kstdio::kprintf(const char *format, ...) {
    va_list arguments;
    va_start(arguments,format);
    while (*format != '\0') {
        if (*format != '%') {
            terminal_text.terminal_putchar(*format);
        } else {
            format++;
            if (*format == 'u') {
                uint32_t unsigned_number = va_arg(arguments,uint32_t);
                kprintf_unsigned(unsigned_number);
            } else if (*format == 'd') {
                int32_t integer_number = va_arg(arguments,uint32_t);
                kprintf_int(integer_number);
            } else if (*format == 'x') {
                uint32_t unsigned_number = va_arg(arguments,uint32_t);
                kprintf_hex(unsigned_number);
            } else if (*format == 's') {
                char *string = va_arg(arguments,char *);
                terminal_text.terminal_putstring(string);
            } 
        }
        format++;
    }
    va_end(arguments);
}