#include <lib/types.hpp>

#define MAX_COL 80
#define MAX_ROW 25 

class Terminal {
private:
    uint16_t cursor_x, cursor_y;
    void move_cursor();
    void scroll_screen();
public:
    Terminal();
    void clear_screen();
    void terminal_putchar(char c);
    void terminal_putstring(const char *str);
};  