#include <lib/types.hpp>
#include <arch/interrupt.hpp>

namespace PIT {
    void initialized_pit();
    void pit_callback(registers *current_register);
};