#include <lib/types.hpp>

extern "C" {
    struct registers {
        uint64_t r15;
        uint64_t r14;
        uint64_t r13;
        uint64_t r12;
        uint64_t r11;
        uint64_t r10;
        uint64_t r9;
        uint64_t r8;
        uint64_t rbp;
        uint64_t rdi;
        uint64_t rsi;
        uint64_t rdx;
        uint64_t rcx;
        uint64_t rbx;
        uint64_t rax;
        uint32_t error_code;
        uint32_t int_no;
        uint64_t rip;
        uint64_t cs;
        uint64_t rflags;
        uint64_t rsp;
        uint64_t ss;
    };

    void interrupt_handler(registers *current_register);
}

namespace interrupt {
    struct IDT {
        uint16_t offset_lower;
        uint16_t selector;
        uint8_t always_zero;
        uint8_t attribute_type;
        uint16_t offset_mid;
        uint32_t offset_higher;
    }__attribute__((packed));

    struct IDT_ptr {
        uint16_t limit;
        uint16_t base;
    }__attribute__((packed));
    
    void remap_PIC();
    void initialize_idt();
    void uninstall_interrupt_handler(int interrupt_number);
    void install_interrupt_handler(int interrupt_number,void (*handler)(registers *current_register));
    void set_idt_gate(uint8_t num,uint32_t base,uint16_t selector,uint16_t flags);
}

extern "C" {
    void exception_handler(registers *current_registers);
}

namespace exception {
    void initialize_exception();
    void exception_uninstall(uint32_t exception_number);
    void exception_install(uint32_t exception_number,void (*handler)(registers *current_register));
}