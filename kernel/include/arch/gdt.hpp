#include <lib/types.hpp>

namespace GDT {
    struct GDT_entry {
        uint16_t limit_low;
        uint16_t base_low;
        uint8_t base_middle;
        uint8_t access;
        uint8_t granularity;
        uint8_t base_high;
    }__attribute__((packed)); 

    struct GDT_ptr {
        uintptr_t base;
        uintptr_t limit;
    }__attribute__((packed));

    void initialize_gdt();
    void set_gdt_gate(uint32_t number,uint32_t base,uint32_t limit, uint8_t access, uint8_t granularity);
};
