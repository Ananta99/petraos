#include <lib/types.hpp>

namespace kstdlib {
    void *memset(void *destination,int value,size_t size);
    void *memcpy(void *destination, void *source,size_t size);
    void *memmove(void *destination, void *source,size_t size);
}  
