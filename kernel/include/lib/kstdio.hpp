#include <lib/types.hpp>

namespace kstdio {
    void kprintf_unsigned(uint32_t value);
    void kprintf_int(int32_t value);
    void kprintf_hex(int32_t hex);
    void kprintf(const char *format, ...);
};