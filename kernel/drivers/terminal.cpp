#include <drivers/terminal.hpp>
#include <arch/ports.hpp>

#define BLACK_COLOR 0
#define WHITE_COLOR 15
#define MASK_FOREGROUND 4
#define MASK_BACKGROUND 0x0F

#define SPACE ' '
#define RETURN '\r'
#define NEW_LINE '\n'
#define BACKSPACE '\b'
#define HORIZONTAL_TAB '\t'
#define IS_ALPHANUMERIC(c) c >= ' '

static uint16_t *terminal_memory = (uint16_t*)(0xB8000);

Terminal::Terminal() {
    cursor_x = 0;
    cursor_y = 0;
}

void Terminal::move_cursor() {
    uint16_t cursor_location = cursor_y * 80 + cursor_x;
    outb(0x3D4,14);
    outb(0x3D5,cursor_location >> 8);
    outb(0x3D4,15);
    outb(0x3D5,cursor_location);
}

void Terminal::scroll_screen() {
    uint16_t attribute = (BLACK_COLOR << MASK_FOREGROUND) | (15 & MASK_BACKGROUND);
    uint16_t blank = SPACE | (attribute << 8);
    size_t clear_upper = (MAX_ROW - 1) * MAX_COL;
    size_t last_line = MAX_ROW * MAX_COL;
    if (cursor_y < 25) 
        return;
    for (size_t i = 0; i < clear_upper; i++)
            terminal_memory[i] = terminal_memory[i + MAX_COL];
    for (size_t i = 0; i < last_line; i++)
        terminal_memory[i] = blank;
    cursor_y = 24; // Move to last line
}

void Terminal::terminal_putchar(char c) {
    volatile uint16_t * memory_location;
    uint16_t attribute = (BLACK_COLOR << MASK_FOREGROUND) | (15 & MASK_BACKGROUND);

    if (IS_ALPHANUMERIC(c)) {
         memory_location = terminal_memory + (cursor_y * 80 + cursor_x);
        *memory_location = c | (attribute << 8);
        cursor_x++;
    }

    if (cursor_x && (c == BACKSPACE))
        cursor_x--;

    if (c == NEW_LINE) {
        cursor_y++;
        cursor_x = 0;
    }

    if (c == RETURN) 
        cursor_x = 0;

    if (c == HORIZONTAL_TAB) 
        cursor_x = (cursor_x + 8) & ~(8-1);

    if (cursor_x >= MAX_COL) {
        cursor_y++;
        cursor_x = 0;
    }

    scroll_screen(); // scroll screen if screen is full
    move_cursor(); // move cursor to next position
}

void Terminal::terminal_putstring(const char *str) {
    for (size_t i = 0; str[i] != '\0'; i++)
        terminal_putchar(str[i]);
}

void Terminal::clear_screen() {
    uint16_t attribute = (BLACK_COLOR << MASK_FOREGROUND) | (15 & MASK_BACKGROUND);
    uint16_t blank = SPACE | (attribute << 8);

    for (size_t i = 0; i < MAX_COL * MAX_ROW; i++)
        terminal_memory[i] = blank;
    move_cursor();
}