G++PARAMS =  -Ikernel/include -ffreestanding  -O2 -Wall -Wextra
G++ = x86_64-elf-g++
ASM = nasm -felf64
GCCLINKING = -ffreestanding -O2 -nostdlib

KERNEL_OBJS = $(patsubst %.asm,%.o,$(wildcard kernel/arch/*.asm))
KERNEL_OBJS += $(patsubst %.cpp,%.o,$(wildcard kernel/arch/*.cpp))
KERNEL_OBJS += $(patsubst %.cpp,%.o,$(wildcard kernel/lib/*.cpp))
KERNEL_OBJS += $(patsubst %.cpp,%.o,$(wildcard kernel/drivers/*.cpp))


kernel/arch/%.o : kernel/arch/%.asm
	$(ASM) $< -o $@

kernel/arch/%.o : kernel/arch/%.cpp
	$(G++) -c $< -o $@ $(G++PARAMS)

 kernel/lib/%.o :  kernel/lib/%.cpp
	$(G++) -c $< -o $@ $(G++PARAMS)

kernel/drivers/%.o : kernel/drivers/%.cpp
	$(G++) -c $< -o $@ $(G++PARAMS)

PetraOS.bin: linker.ld  $(KERNEL_OBJS)
	ld -n -T $< -o $@ $(G++LINKING) $(KERNEL_OBJS)

PetraOS.iso: PetraOS.bin
	mkdir -p iso/boot/grub
	cp PetraOS.bin iso/boot/PetraOS.bin
	echo 'set timeout=0'> iso/boot/grub/grub.cfg
	echo 'set default=0' >> iso/boot/grub/grub.cfg
	echo ''
	echo 'menuentry "My Operating System" {' >> iso/boot/grub/grub.cfg
	echo '	multiboot2 /boot/PetraOS.bin' >> iso/boot/grub/grub.cfg
	echo '  boot' >> iso/boot/grub/grub.cfg
	echo '}' >> iso/boot/grub/grub.cfg
	grub-mkrescue -o PetraOS.iso iso 
	rm -rf iso
run:
	qemu-system-x86_64 -cdrom PetraOS.iso	

clean:
	rm kernel/arch/*\.o
	rm kernel/drivers/*\.o
	rm kernel/lib/*\.o
	rm *\.iso
	rm *\.bin
